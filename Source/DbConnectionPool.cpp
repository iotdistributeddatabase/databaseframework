/*
 * DatabaseFramework
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <DbConnectionPool.hpp>
#include "ConnectionDropper.hpp"
#include <QThread>
#include <QObject>
#include <algorithm>
#include <QDebug>

namespace Database
{

std::unique_ptr<DbConnectionPool> DbConnectionPool::connPool_ = nullptr;

DbConnectionPool::DbConnectionPool(const QString& driverType,
                               const QString& databaseName,
                               const QString& hostName,
                               const uint16_t port,
                               const QString& userName,
                               const QString& password,
                               const QString& connectOptions)
    : driverType_(driverType), databaseName_(databaseName),
      hostName_(hostName), port_(port), userName_(userName),
      password_(password), connectOptions_(connectOptions),
      isSynchronized_(false)
{

}

void DbConnectionPool::createGlobalInstance(const QString& driverType,
                                          const QString& databaseName,
                                          const QString& hostName,
                                          const uint16_t port,
                                          const QString& userName,
                                          const QString& password,
                                          const QString& connectOptions)
{
    connPool_ = std::make_unique<DbConnectionPool>(driverType, databaseName,
                                                 hostName, port, userName,
                                                 password, connectOptions);
}

DbConnectionPool *DbConnectionPool::globalInstance()
{
    return connPool_.get();
}

QSqlDatabase DbConnectionPool::getConnection() const
{
    QThread* thread = QThread::currentThread();
    QString threadStr;
    threadStr.sprintf("%p", static_cast<void*>(thread));
    QSqlDatabase conn = QSqlDatabase::database(threadStr);
    if(not conn.isValid())
    {
        conn = QSqlDatabase::addDatabase(driverType_, threadStr);
        conn.setDatabaseName(databaseName_);
        conn.setHostName(hostName_);
        conn.setPort(port_);
        conn.setUserName(userName_);
        conn.setPassword(password_);
        conn.setConnectOptions(connectOptions_);

        QObject::connect(thread, &QThread::finished,
                         droper(), &ConnectionDropper::drop);
        if(not conn.open())
        {
            qCritical() << "DbConnection: server could not setup connection to database " << databaseName_;
        }
    }
    return conn;
}

void DbConnectionPool::clear()
{
    auto list = QSqlDatabase::connectionNames();
    std::for_each(list.begin(), list.end(),
                  [](const QString &s){ QSqlDatabase::removeDatabase(s); });
}

bool DbConnectionPool::isSynchronized()
{
    return isSynchronized_;
}

void DbConnectionPool::setIsSynchronized(const bool isSynchronized)
{
    isSynchronized_ = isSynchronized;
}

} // namespace Database
