/*
 * DatabaseFramework
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATABASE_CONNECTIONPOOL_HPP
#define DATABASE_CONNECTIONPOOL_HPP

#include <memory>
#include <cstdint>
#include <QString>
#include <QtSql/QSqlDatabase>
#include <atomic>

namespace Database
{

class DbConnectionPool
{
public:
    DbConnectionPool() = delete;
    DbConnectionPool(const DbConnectionPool&) = delete;
    DbConnectionPool& operator=(const DbConnectionPool&) = delete;
    DbConnectionPool(DbConnectionPool&&) = delete;
    DbConnectionPool& operator=(DbConnectionPool&&) = delete;
    explicit DbConnectionPool(const QString &driverType,
                            const QString &databaseName,
                            const QString &hostName,
                            const uint16_t port,
                            const QString &userName,
                            const QString &password,
                            const QString &connectOptions);

    static void createGlobalInstance(const QString& driverType,
                                     const QString& databaseName,
                                     const QString& hostName,
                                     const uint16_t port,
                                     const QString& userName,
                                     const QString& password,
                                     const QString& connectOptions);
    static DbConnectionPool* globalInstance();

    QSqlDatabase getConnection() const;
    void clear();
    bool isSynchronized();
    void setIsSynchronized(const bool isSynchronized);

private:
    const QString driverType_;
    const QString databaseName_;
    const QString hostName_;
    const uint16_t port_;
    const QString userName_;
    const QString password_;
    const QString connectOptions_;
    std::atomic_bool isSynchronized_;

    static std::unique_ptr<DbConnectionPool> connPool_;
};

} // namespace Database

#endif // DATABASE_CONNECTIONPOOL_HPP
